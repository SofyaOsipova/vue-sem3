export default {
    state() {
        return {
            prods: [],
            detailProd: {}
        }
    },
    actions: {
        async fetchProds(ctx) {
            const res = await fetch("./main.json");
            const prods = await res.json();
            this.prods = prods;
            ctx.commit('updateProds', prods)
        },
        async filter({ state, commit }, price) {
            const res = await fetch("./main.json");
            const prods = await res.json();
            state.prods = prods;
            console.log(state.prods);
            commit('filterProds', price);
        }
        
    },
    mutations: {
        updateProds(state, prods) {
            state.prods = prods
        },
        updateDetail(state, detailProd) {
            state.detailProd = detailProd
        },
        sortAscending(state) {
            let prod = state.prods;
            prod.sort(function(a, b) {
                return parseFloat(a.price) - parseFloat(b.price);
            });
            state.prods = prod;
        },
        sortDescending(state) {
            let prod = state.prods;
            prod.sort(function(a, b) {
                return parseFloat(b.price) - parseFloat(a.price);
            });
            state.prods = prod;
        },
        filterProds(state, price) {
            console.log(111, state.prods);
            let prods = [];
            let min = price[0];
            let max = price[1];
            for (let item of state.prods) {
                if (min <= item.price && item.price <= max) {
                    prods.push(item);
                }
            }
            state.prods = prods;
            console.log(222, state.prods);
        }
    },
    getters: {
        allProds(state) {
            return state.prods
        },
        getById: state => id => state.prods.find(prod => prod.id == id)
    },
}