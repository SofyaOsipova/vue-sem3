import Vue from 'vue'
import VueRouter from 'vue-router'
import RestPage from '@/components/RestPage'
import ProductPage from '@/components/ProductPage'

Vue.use(VueRouter)

const routes = [
    
    { path: '/', component: RestPage, name: 'home' },
    { path: '/product/:id', name: 'prod', component: ProductPage, params: true },
    // {
    //     path: '/',
    //     name: 'home',
    //     component: HomeView
    // },
    // {
    //     path: '/about',
    //     name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
    //     component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
    // }
    ]

    const router = new VueRouter({
      mode: 'history',
      base: process.env.BASE_URL,
      routes
    })

export default router
